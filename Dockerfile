FROM alpine:3.4

RUN apk --update add nginx php5-fpm && \
    mkdir -p /run/nginx && \
	apk add --update --no-cache curl py-pip && \
	pip install awscli && \
	aws --version
	

COPY . .

CMD ["sh", "run.sh"]